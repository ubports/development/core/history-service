set(reader_SRCS main.cpp)

include_directories(
    ${CMAKE_SOURCE_DIR}/src
    )

add_executable(lomiri-history-reader ${reader_SRCS})

target_link_libraries(lomiri-history-reader Qt5::Core lomirihistoryservice)
