-- Allows to disable triggers when needed
CREATE TABLE IF NOT EXISTS "disabled_triggers" (
        "name"	TEXT NOT NULL
);

-- Drop all triggers

DROP TRIGGER text_events_delete_trigger;
DROP TRIGGER text_events_insert_trigger;
DROP TRIGGER text_events_update_trigger;
DROP TRIGGER text_threads_delete_trigger;
DROP TRIGGER threads_delete_trigger;
DROP TRIGGER voice_events_delete_trigger;
DROP TRIGGER voice_events_insert_trigger;
DROP TRIGGER voice_events_update_trigger;
DROP TRIGGER voice_threads_delete_trigger;

-- Recreate all triggers with new condition

CREATE TRIGGER voice_events_insert_trigger  AFTER INSERT ON voice_events
FOR EACH ROW WHEN NOT exists(SELECT name FROM disabled_triggers WHERE name='voice_events_insert_trigger')
BEGIN
    UPDATE threads SET count=(SELECT count(eventId) FROM voice_events WHERE
        accountId=new.accountId AND
        threadId=new.threadId)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=1;
    UPDATE threads SET unreadCount=(SELECT count(eventId) FROM voice_events WHERE
        accountId=new.accountId AND threadId=new.threadId AND newEvent='1')
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=1;
    UPDATE threads SET lastEventId=(SELECT eventId FROM voice_events WHERE
        accountId=new.accountId AND
        threadId=new.threadId
        ORDER BY timestamp DESC LIMIT 1)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=1;
    UPDATE threads SET lastEventTimestamp=(SELECT timestamp FROM voice_events WHERE
        accountId=new.accountId AND
        threadId=new.threadId
        ORDER BY timestamp DESC LIMIT 1)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=1;
END;
CREATE TRIGGER voice_events_update_trigger  AFTER UPDATE ON voice_events
FOR EACH ROW WHEN NOT exists(SELECT name FROM disabled_triggers WHERE name='voice_events_update_trigger')
BEGIN
    UPDATE threads SET count=(SELECT count(eventId) FROM voice_events WHERE
        accountId=new.accountId AND
        threadId=new.threadId)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=1;
    UPDATE threads SET unreadCount=(SELECT count(eventId) FROM voice_events WHERE
        accountId=new.accountId AND threadId=new.threadId AND newEvent='1')
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=1;
    UPDATE threads SET lastEventId=(SELECT eventId FROM voice_events WHERE
        accountId=new.accountId AND
        threadId=new.threadId
        ORDER BY timestamp DESC LIMIT 1)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=1;
    UPDATE threads SET lastEventTimestamp=(SELECT timestamp FROM voice_events WHERE
        accountId=new.accountId AND
        threadId=new.threadId
        ORDER BY timestamp DESC LIMIT 1)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=1;
END;
CREATE TRIGGER voice_events_delete_trigger  AFTER DELETE ON voice_events
FOR EACH ROW WHEN NOT exists(SELECT name FROM disabled_triggers WHERE name='voice_events_delete_trigger')
BEGIN
    UPDATE threads SET count=(SELECT count(eventId) FROM voice_events WHERE
        accountId=old.accountId AND
        threadId=old.threadId)
        WHERE accountId=old.accountId AND threadId=old.threadId AND type=1;
    UPDATE threads SET unreadCount=(SELECT count(eventId) FROM voice_events WHERE
        accountId=old.accountId AND threadId=old.threadId AND newEvent='1')
        WHERE accountId=old.accountId AND threadId=old.threadId AND type=1;
    UPDATE threads SET lastEventId=(SELECT eventId FROM voice_events WHERE
        accountId=old.accountId AND
        threadId=old.threadId
        ORDER BY timestamp DESC LIMIT 1)
        WHERE accountId=old.accountId AND threadId=old.threadId AND type=1;
    UPDATE threads SET lastEventTimestamp=(SELECT timestamp FROM voice_events WHERE
        accountId=old.accountId AND
        threadId=old.threadId
        ORDER BY timestamp DESC LIMIT 1)
        WHERE accountId=old.accountId AND threadId=old.threadId AND type=1;
END;
CREATE TRIGGER threads_delete_trigger AFTER DELETE ON threads
FOR EACH ROW WHEN NOT exists(SELECT name FROM disabled_triggers WHERE name='threads_delete_trigger')
BEGIN
    DELETE FROM thread_participants WHERE
        accountId=old.accountId AND
        threadId=old.threadId AND
        type=old.type;
    DELETE FROM chat_room_info WHERE
        accountId=old.accountId AND
        threadId=old.threadId AND
        type=old.type;
END;
CREATE TRIGGER text_events_insert_trigger AFTER INSERT ON text_events
FOR EACH ROW WHEN new.messageType!=2 AND NOT exists(SELECT name FROM disabled_triggers WHERE name='text_events_insert_trigger')
BEGIN
    UPDATE threads SET count=(SELECT count(eventId) FROM text_events WHERE
        accountId=new.accountId AND
        threadId=new.threadId AND
        messageType!=2)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=0;
    UPDATE threads SET unreadCount=(SELECT count(eventId) FROM text_events WHERE
        accountId=new.accountId AND threadId=new.threadId AND newEvent='1' AND messageType!=2)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=0;
    UPDATE threads SET lastEventId=(SELECT eventId FROM text_events WHERE
        accountId=new.accountId AND
        threadId=new.threadId AND
        messageType!=2
        ORDER BY timestamp DESC LIMIT 1)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=0;
    UPDATE threads SET lastEventTimestamp=(SELECT timestamp FROM text_events WHERE
        accountId=new.accountId AND
        threadId=new.threadId AND
        messageType!=2
        ORDER BY timestamp DESC LIMIT 1)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=0;
END;
CREATE TRIGGER text_events_update_trigger AFTER UPDATE ON text_events
FOR EACH ROW WHEN new.messageType!=2 AND NOT exists(SELECT name FROM disabled_triggers WHERE name='text_events_update_trigger')
BEGIN
    UPDATE threads SET count=(SELECT count(eventId) FROM text_events WHERE
        accountId=new.accountId AND
        threadId=new.threadId AND
        messageType!=2)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=0;
    UPDATE threads SET unreadCount=(SELECT count(eventId) FROM text_events WHERE
        accountId=new.accountId AND threadId=new.threadId AND newEvent='1' AND messageType!=2)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=0;
    UPDATE threads SET lastEventId=(SELECT eventId FROM text_events WHERE
        accountId=new.accountId AND
        threadId=new.threadId AND
        messageType!=2
        ORDER BY timestamp DESC LIMIT 1)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=0;
    UPDATE threads SET lastEventTimestamp=(SELECT timestamp FROM text_events WHERE
        accountId=new.accountId AND
        threadId=new.threadId AND
        messageType!=2
        ORDER BY timestamp DESC LIMIT 1)
        WHERE accountId=new.accountId AND threadId=new.threadId AND type=0;
END;
CREATE TRIGGER text_events_delete_trigger AFTER DELETE ON text_events
FOR EACH ROW WHEN old.messageType!=2 AND NOT exists(SELECT name FROM disabled_triggers WHERE name='text_events_delete_trigger')
BEGIN
    UPDATE threads SET count=(SELECT count(eventId) FROM text_events WHERE
        accountId=old.accountId AND
        threadId=old.threadId AND
        messageType!=2)
        WHERE accountId=old.accountId AND threadId=old.threadId AND type=0;
    UPDATE threads SET unreadCount=(SELECT count(eventId) FROM text_events WHERE
        accountId=old.accountId AND threadId=old.threadId AND newEvent='1' AND messageType!=2)
        WHERE accountId=old.accountId AND threadId=old.threadId AND type=0;
    UPDATE threads SET lastEventId=(SELECT eventId FROM text_events WHERE
        accountId=old.accountId AND
        threadId=old.threadId AND
        messageType!=2
        ORDER BY timestamp DESC LIMIT 1)
        WHERE accountId=old.accountId AND threadId=old.threadId AND type=0;
    UPDATE threads SET lastEventTimestamp=(SELECT timestamp FROM text_events WHERE
        accountId=old.accountId AND
        threadId=old.threadId AND
        messageType!=2
        ORDER BY timestamp DESC LIMIT 1)
        WHERE accountId=old.accountId AND threadId=old.threadId AND type=0;
    DELETE from text_event_attachments WHERE
        accountId=old.accountId AND
        threadId=old.threadId AND
        eventId=old.eventId;
END;
CREATE TRIGGER text_threads_delete_trigger AFTER DELETE ON threads
FOR EACH ROW WHEN old.type=0 AND NOT exists(SELECT name FROM disabled_triggers WHERE name='text_threads_delete_trigger')
BEGIN
    DELETE FROM text_events WHERE
        accountId=old.accountId AND
        threadId=old.threadId;
END;
CREATE TRIGGER voice_threads_delete_trigger AFTER DELETE ON threads
FOR EACH ROW WHEN old.type=1 AND NOT exists(SELECT name FROM disabled_triggers WHERE name='voice_threads_delete_trigger')
BEGIN
    DELETE FROM voice_events WHERE
        accountId=old.accountId AND
        threadId=old.threadId;
END;
