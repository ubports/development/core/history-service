/*
 * Copyright (C) 2022 Ubports Foundation.
 *
 * Authors:
 *  Lionel Duboeuf <lduboeuf@ouvaton.org>
 *
 * This file is part of lomiri-history-service.
 *
 * lomiri-history-service is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * lomiri-history-service is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "emblemcountmanager.h"
#include "historydaemon.h"
#include "types.h"

#include <QDBusInterface>
#include <QDebug>

#define LAUNCHER_SERVICE "com.lomiri.Shell.Launcher"
#define LAUNCHER_ITEM_IFACE "com.lomiri.Shell.Launcher.Item"
#define MESSAGING_APP_EMBLEM_OBJECT_PATH "/com/lomiri/Shell/Launcher/messaging_2Dapp"
#define DIALER_APP_EMBLEM_OBJECT_PATH "/com/lomiri/Shell/Launcher/dialer_2Dapp"

EmblemCountManager::EmblemCountManager(QObject *parent) :
    QObject(parent),
    mWatcher(LAUNCHER_SERVICE, QDBusConnection::sessionBus(),
                              QDBusServiceWatcher::WatchForRegistration)
{
    connect(&mWatcher, &QDBusServiceWatcher::serviceRegistered, this, [this]() {
        updateCounters();
    });

    updateCounters();
    qDebug() << "emblem counters initialized";
}

EmblemCountManager::~EmblemCountManager()
{
}

QDBusInterface *EmblemCountManager::getEmblemCounter(History::EventType type)
{
    QDBusInterface* emblemCounter = nullptr;
    switch (type) {
    case History::EventTypeText:
        emblemCounter = new QDBusInterface( LAUNCHER_SERVICE, MESSAGING_APP_EMBLEM_OBJECT_PATH,
                                          LAUNCHER_ITEM_IFACE);
        break;
    case History::EventTypeVoice:
        emblemCounter = new QDBusInterface( LAUNCHER_SERVICE, DIALER_APP_EMBLEM_OBJECT_PATH,
                                          LAUNCHER_ITEM_IFACE);
        break;
    default:
        qWarning() << "fail to get emblem counter dbus interface, got event type null";
        break;
    }

    return emblemCounter;
}

void EmblemCountManager::updateCounter(History::EventType type) {

   QDBusInterface* emblemCounter = getEmblemCounter(type);
   if (emblemCounter == nullptr) {
       return;
   }

   // only update counter if app is pinned
   if (emblemCounter->isValid() && emblemCounter->property("count").isValid()) {
       int count = HistoryDaemon::instance()->getUnreadCount(type);
       // when messaging-app/dialer app is not pinned, launcher service only creates the emblem on countVisible property,
       // in that case the count is initialized to zero. This allows to force Item creation before setting the count
       emblemCounter->setProperty("countVisible", count > 0);
       emblemCounter->setProperty("count", count);
   }
   emblemCounter->deleteLater();
}

void EmblemCountManager::updateCounters()
{
    updateCounter(History::EventTypeText);
    updateCounter(History::EventTypeVoice);
}

